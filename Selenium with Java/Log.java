import org.junit.Test;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.testng.Assert.assertEquals;

public class Log
{
        @Test
        public void testLog() {
            System.setProperty("webdriver.edge.driver\",\"C:\\\\Users\\\\AXELL\\\\Downloads\\\\msedgedriver.exe");
            WebDriver driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.get("https://formy-project.herokuapp.com/form");

            WebDriverWait w = new WebDriverWait(driver,5);

            formPage fp = new formPage(driver);
            fp.name().sendKeys("Axell");
            fp.last().sendKeys("Lopez");
            fp.job().sendKeys("IT Trainee");
            fp.edu().get(0).click();
            fp.sex().get(2).click();
            fp.optYears().click();
            fp.SelectYears().get(0).click();
            fp.date().sendKeys("09/10/2021"+Keys.RETURN);
            fp.submit().click();
            w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
            assertEquals(driver.findElement(By.cssSelector("[align='center']")).getText(), "Thanks for submitting your form");
        }
}
