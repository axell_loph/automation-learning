import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.image.ImagingOpException;
import java.time.Duration;
import java.util.ArrayList;

public class excelTest
{
    WebDriver driver;

    @Before
    public void setting()
    {
        System.setProperty("webdriver.edge.driver", "F:\\EdgeDriver\\msedgedriver.exe");
        driver= new EdgeDriver();
        driver.get("http://formy-project.herokuapp.com/form");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testForm() throws InterruptedException, ImagingOpException
    {
        excel ExcelData= new excel();
        ArrayList<String> data= ExcelData.getData();

        String name= data.get(1);
        String last= data.get(2);
        String job= data.get(3);
        String edu= data.get(4);
        String s= data.get(5);
        String yExp= data.get(6);

        formpage formp= new formpage();
        formp.submform(driver, name, last, job, edu, s, yExp);

        WebDriverWait w= new WebDriverWait(driver, 10);
        w.until(ExpectedCondition.visibilityOfElementLocated(By.cssSelector(".alert")));
        String confirmM= formp.getConfirm(driver);
        Assert.assertEquals(confirmM, "Thanks for submitting your form.");
    }
}
