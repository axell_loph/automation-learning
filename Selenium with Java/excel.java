import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

import java.awt.image.ImagingOpException;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class excel
{
    String name;
    String last;
    String job;
    String edu;
    String sex;
    String yearExp;
    String filePath= "F:\\dataex.xls";

    public ArrayList<String> getData() throws ImagingOpException
    {
        File file= new File(filePath);
        FileInputStream fis= new FileInputStream(file);
        HSSFWorkbook workbk= new HSSFWorkbook(fis);
        HSSFSheet sheet= workbk.getSheet("1");

        Iterator<Row> row= sheet.rowIterator();
        row.next();
        ArrayList<String> data= new ArrayList<String>();
        HSSFRow eachRow= sheet.getRow(1);

        name= eachRow.getCell(1).getStringCellValue();
        data.add(name);
        last= eachRow.getCell(2).getStringCellValue();
        data.add(last);
        job= eachRow.getCell(3).getStringCellValue();
        data.add(job);
        edu= eachRow.getCell(4).getStringCellValue();
        data.add(edu);
        sex= eachRow.getCell(5).getStringCellValue();
        data.add(sex);
        yearExp= eachRow.getCell(6).getStringCellValue();
        data.add(yearExp);
        return data;
    }
}