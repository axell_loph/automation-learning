import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WebForm
{
    WebDriver driver;
    public WebForm(WebDriver driver)
    {
        this.driver= driver;
    }

    private By name= By.ccsSelector("input[placeholder*='name']");
    private By last= By.ccsSelector("input[placeholder*='last']");
    private By job= By.ccsSelector("input[placeholder*='job']");
    private By educa= By.ccsSelector("input[type='radio']");
    private By sex= By.ccsSelector("input[placeholder*='name']");
    private By choyears= By.id("select-menu");
    private By yearsEx= By.xpath("//select/option");
    private By date= By.id("datepicker");
    private By submit= By.ccsSelector("a[class*='btn-primary']");
    private By adv= By.ccsSelector("div[role='alert']");

    public WebElement name()
    {
        return driver.findElement(name);
    }
    public WebElement last()
    {
        return driver.findElement(last);
    }
    public WebElement job()
    {
        return driver.findElement(job);
    }
    public List<WebElement> educa()
    {
        return driver.findElements(educa);
    }
    public List<WebElement> sex()
    {
        return driver.findElements(sex);
    }
    public List<WebElement> SelectYears()
    {
        return driver.findElements(choyears);
    }
    public WebElement optYears()
    {
        return driver.findElement(yearsEx);
    }
    public WebElement date()
    {
        return driver.findElement(date);
    }
    public WebElement submit()
    {
        return driver.findElement(submit);
    }
    public WebElement message()
    {
        return driver.findElement(adv);
    }

}