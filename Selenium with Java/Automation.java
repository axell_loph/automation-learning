import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;

public class Automation
{
    public static void main(String[] args)
    {
        // TODO Auto-generated method stub

        System.setProperty("webdriver.edge.driver","C:\\Users\\AXELL\\Downloads\\msedgedriver.exe");
        WebDriver driver = new EdgeDriver();

        driver.get("https://formy-project.herokuapp.com/form");

        //Name Form
        WebElement fname = driver.findElement(By.id("first-name"));
        fname.sendKeys("Axell");

        WebElement lname = driver.findElement(By.id("last-name"));
        lname.sendKeys("Lopez");

        WebElement job = driver.findElement(By.id("job-title"));
        job.sendKeys("IT Trainee");

        //Education
        WebElement btn = driver.findElement(By.xpath("//*[@id=\"radio-button-2\"]"));
        btn.click();

        //Sex
        WebElement box = driver.findElement(By.xpath("//*[@id=\"checkbox-1\"]"));
        box.click();

        //Experience
        WebElement years = driver.findElement(By.id("select-menu"));
        years.click();
        WebElement years2 = driver.findElement(By.xpath("//*[@id=\"select-menu\"]/option[3]"));
        years2.click();

        //Date
        WebElement dateField = driver.findElement(By.id("datepicker"));
        dateField.sendKeys("09/10/2020"); // mm/dd/yyyy
        dateField.sendKeys(Keys.RETURN);

        //Submit form
        driver.findElement(By.xpath("/html/body/div/form/div/div[8]/a")).click();
    }
}


