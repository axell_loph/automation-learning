import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class formpage
{
    public void submform (WebDriver driver, String name, String last, String job,
                          String edu, String s, String yExp) throws InterruptedException
    {

        WebElement firstn= driver.findElement(By.id("first_name"));
        firstn.sendKeys(name);
        WebElement lastn= driver.findElement(By.id("last_name"));
        lastn.sendKeys(last);
        WebElement jobt= driver.findElement(By.id("job_title"));
        jobt.sendKeys(job);
        WebElement levEdu= driver.findElement(By.id("radio_button"+edu));
        levEdu.click();
        WebElement sex= driver.findElement(By.xpath("checkbox-"+s));
        sex.click();
        WebElement yearsExp= driver.findElement(By.xpath("//select/option["+yExp+"]"));
        yearsExp.click();
        WebElement date= driver.findElement(By.id("datepicker"));
        date.click();
        WebElement today= driver.findElement(By.xpath("//tr/td[@class= 'today day']"));
        today.click();
        WebElement submit= driver.findElement(By.xpath("//a[@role= 'button']"));
        submit.click();
    }

    public String getConfirm(WebDriver driver)
    {
        WebElement confirmTitle= driver.findElement(By.tagName("h1"));
        String title= confirmTitle.getText();
        return title;
    }
}

