import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;

public class formpage
{
    public void submform (WebDriver driver, String name, String last, String job,
                          String edu, String s, String yExp) throws IOException
    {

        WebElement firstn= driver.findElement(By.id("first_name"));
        firstn.sendKeys(name);
        WebElement lastn= driver.findElement(By.id("last_name"));
        lastn.sendKeys(last);
        WebElement jobt= driver.findElement(By.id("job_title"));
        jobt.sendKeys(job);
        WebElement levEdu= driver.findElement(By.id("radio_button"+edu));
        levEdu.click();
        WebElement sex= driver.findElement(By.xpath("checkbox-"+s));
        sex.click();
        getScreenshot(driver, "F:\\Selenium_ex6\\selectsex.png");
        WebElement yearsExp= driver.findElement(By.xpath("//select/option["+yExp+"]"));
        yearsExp.click();
        getScreenshot(driver, "F:\\Selenium_ex6\\selectyearExp.png");
        WebElement date= driver.findElement(By.id("datepicker"));
        date.click();
        WebElement today= driver.findElement(By.xpath("//tr/td[@class= 'today day']"));
        today.click();
        getScreenshot(driver, "F:\\Selenium_ex6\\selectdate.png");
        WebElement submit= driver.findElement(By.xpath("//a[@role= 'button']"));
        submit.click();
    }

    public String getConfirm(WebDriver driver)
    {
        WebElement confirmTitle= driver.findElement(By.tagName("h1"));
        String title= confirmTitle.getText();
        return title;
    }

    public void getScreenshot(WebDriver driver, String path) throws IOException
    {
        File scr= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scr, new File(path) );
    }
}

