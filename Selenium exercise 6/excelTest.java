
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;

public class excelTest
{
    WebDriver driver;

    @Before
    public void setting()
    {
        System.setProperty("webdriver.edge.driver", "F:\\EdgeDriver\\msedgedriver.exe");
        driver= new EdgeDriver();
        //File screenshootFile= ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        //FileUtils.copyFile(screenshootFile, new File(".//screenshot/screen.png"));
        driver.get("http://formy-project.herokuapp.com/form");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testForm() throws InterruptedException, IOException
    {
        excel ExcelData= new excel();
        ArrayList<String> data= ExcelData.getData();

        String name= data.get(1);
        String last= data.get(2);
        String job= data.get(3);
        String edu= data.get(4);
        String s= data.get(5);
        String yExp= data.get(6);

        formpage formp= new formpage();
        formp.submform(driver, name, last, job, edu, s, yExp);

        WebDriverWait w= new WebDriverWait(driver, 1000);
        w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".alert")));
        String confirmM= formp.getConfirm(driver);
        Assert.assertEquals(confirmM, "Thanks for submitting your form.");
    }
}
